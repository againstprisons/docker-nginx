FROM alpine:3.7

RUN apk add --no-cache \
  nginx \
  php7-fpm \
  php7-tokenizer \
  php7-simplexml \
  php7-fileinfo \
  php7-tidy \
  php7-mbstring \
  php7-ctype \
  php7-dom \
  php7-xmlreader \
  php7-xsl \
  php7-fileinfo \
  php7-intl \
  php7-session \
  php7-xml \
  php7-pdo \
  php7-pdo_mysql \
  php7-pdo_pgsql \
  php7-mysqli \
  php7-pgsql \
  php7-imagick \
  php7-json \
  supervisor

RUN mkdir -p /var/log/nginx

RUN rm -rf /var/www && mkdir -p /var/www
VOLUME /var/www

RUN rm -rf /etc/nginx && mkdir /etc/nginx
VOLUME /etc/nginx

COPY config/php-fpm.conf /etc/php7/php-fpm.conf
COPY config/php.ini /etc/php7/php.ini

RUN mkdir -p /var/log/supervisor
COPY config/supervisord.conf /etc/supervisor/supervisord.conf
CMD supervisord -c /etc/supervisor/supervisord.conf
