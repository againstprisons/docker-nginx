# docker-nginx

A simple nginx + php7-fpm Docker image based on Alpine.

## Using

**No default nginx config is provided, you must use your own.**

You must place your nginx config in `/etc/nginx`. The supervisord config is set
to start nginx with `/etc/nginx/nginx.conf` as the configuration file.

php7-fpm is listening on `/run/php-fpm.sock`, so you should set your nginx
configuration to use that:

```
fastcgi_pass unix:/run/php-fpm.sock;
```

## License

MIT, see [./LICENSE](./LICENSE)

